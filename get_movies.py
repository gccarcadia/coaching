from selenium import webdriver
from time import sleep
from bs4 import BeautifulSoup
from requests import get as GET
from getpass import getpass


login_email = input("Enter e-mail account: ")
login_pass = getpass("Enter password: ")
course_to_download = input("Enter the name of the course to download: ")
url = "https://www.linkedin.com/learning/login"

options = webdriver.ChromeOptions()
options.add_argument("--start-maximized")
driver = webdriver.Chrome(options=options)
driver.get(url)

sleep(1) # Allow the page to load

# The element is inside another HTML page (Iframe = inline frame)
iframe = driver.find_element_by_class_name("authentication-iframe")
# Switching to the inline frame
driver.switch_to.frame(iframe)
driver.find_element_by_id("session_key-login").send_keys(login_email)

sleep(1) # Allow the page to load

driver.find_element_by_id('session_password-login').send_keys(login_pass)

sleep(1) # Allow the page to load

driver.find_element_by_xpath('//*[@id="btn-primary"]').click()

sleep(5) # Allow the page to load

# Click on the "Saved" courses button
driver.find_element_by_xpath("//li[@class='nav-bar__item nav-bar__item--saved']"
                             "//a[@class='nav-bar__action t-12 t-white t-normal ember-view']").click()

sleep(5) # Allow the page to load

driver.find_element_by_partial_link_text(course_to_download).click()

sleep(5) # Allow the page to load

# Clicking on "Contents" button
text = "//div[@class='artdeco-scrolling-container']//artdeco-tab[text()= 'Contents']"
contents_button = driver.find_element_by_xpath(text).click()

# Getting the HTML page contents to parse with beautiful soup
course_html = driver.page_source
soup = BeautifulSoup(course_html, "html.parser")

# Getting the downloadable chapter names
chapters = soup.findAll("div", {"class": "toc-item__content t-14 t-black t-normal"})
movies_list = []
for item in chapters:
    # Filtering out the items that are "Locked" (unavailable movies) and also the chapter quizzes
    if "Locked" not in item.text:
        if "Chapter Quiz" not in item.text:
            # Removing "In progress" tag from the name of the movies that are in progress
            movies_list.append(item.text.split("\n")[0].split("  ")[0])


def normalize_movie_name(movie_name):
    """
        This function replaces some of the characters in movie names that otherwise
        would be unsuitable for file names (characters like "?")
    """
    normalized_movie_name = movie_name.replace(" ", "_")
    # TODO - Need to find a better solution to this if other non-alphanumeric characters are used
    normalized_movie_name = normalized_movie_name.replace("?", "")
    return normalized_movie_name


def download_movie(movie_name, movie_url):
    # This code downloads the movies
    # TODO - find a way to download HD movies since the resolution is pretty garbage by default
    movie_data = GET(movie_url)
    with open(movie_name, "wb") as movie:
        print("Downloading file: {}".format(movie_name))
        movie.write(movie_data.content)
        print("Done!")


movie_prefix = 0

for movie_name in movies_list:
    driver.find_element_by_partial_link_text(movie_name).click()
    sleep(1)
    current_movie_html_data = driver.page_source
    movie_soup = BeautifulSoup(current_movie_html_data, "html.parser")
    movie_url = movie_soup.find("div", {"class": "video-container"}).find("source").get("src")
    # Building a valid name for the movie - some characters are unsuitable for file names
    download_movie(str(movie_prefix) + "_" + normalize_movie_name(movie_name) + ".mp4", movie_url)
    movie_prefix += 1

driver.quit()
